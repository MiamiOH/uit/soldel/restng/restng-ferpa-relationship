<?php
/*
-----------------------------------------------------------
FILE NAME: Relationship.class.php

Copyright (c) 2018 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Meenakshi Kandasamy

DESCRIPTION:  Class for the Ferpa Relationship RESTng Service

ENVIRONMENT DEPENDENCIES: RESTng

AUDIT TRAIL:

DATE        UniqueID
03/12/2018  Kandasm       Initial File
*/

namespace MiamiOH\RestngFerpaRelationship\Services;


class Relationship extends \MiamiOH\RESTng\Service
{
    private $dataSource = '';
    private $database = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_GEN_PROD'; // secure datasource

    // Inject the datasource object provided by the framework
    public function setDataSource($datasource)
    {
        $this->dataSource = $datasource;
    }

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    // Inject the configuration object provided by the framework
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    public function getRelationship()
    {
        $payload = array();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $relationshipCode = null;

        $dbh = $this->database->getHandle($this->datasource_name);

        if (isset($options['relationshipCode'])) {
            if (ctype_alpha($options['relationshipCode']) && (strlen(trim($options['relationshipCode'])) == 1)) {
                $relationshipCode = strtoupper(trim($options['relationshipCode']));
            } else {
                throw new \Exception('Error: relationshipCode is invalid');
            }
        }
        $query = 'select stvrelt_code,stvrelt_desc from saturn.stvrelt';

        if ($relationshipCode) {
            $results = $dbh->queryall_array($query . ' where upper(stvrelt_code) = ?', $relationshipCode);
        } else {
            $results = $dbh->queryall_array($query . ' order by stvrelt_code');
        }

        foreach ($results as $row) {
            $payload[] = $this->buildRecord($row);
        }

        if (sizeof($payload) <= 0) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        // Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    //build json record from the raw database record
    private function buildRecord($row)
    {
        $record = array(
            'relationshipCode' => $row['stvrelt_code'],
            'relationshipDescription' => $row['stvrelt_desc'],
        );
        return $record;
    }


}
