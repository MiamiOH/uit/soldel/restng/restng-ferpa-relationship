<?php

namespace MiamiOH\RestngFerpaRelationship\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class RelationshipResourceProvider extends ResourceProvider
{

    private $tag = "Ferpa";
    private $dot_path = "Ferpa.Relationship";
    private $s_path = "/ferpa/relationship/v1";
    private $bs_path = '\Relationship';

    public function registerDefinitions(): void
    {
//        $this->addTag(array(
//    'name' => 'Ferpa',
//    'description' => 'Ferpa resources'
//));


            $this->addDefinition(array(
                'name' => $this->dot_path . '.Get.Return.Relationship',
                'type' => 'object',
                'properties' => array(
                    'relationshipCode' => array('type' => 'string', 'description' => 'Code of the relationship'),
                    'relationshipDescription' => array('type' => 'string', 'description' => 'Description of the relationship'),
                ),
            ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Relationship.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Relationship',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Relationship',
            'class' => 'MiamiOH\RestngFerpaRelationship\Services' . $this->bs_path,
            'description' => 'This service provides resources about relationship information for shared application.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
//        'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->dot_path . '.get',
                'description' => 'Return Relationship information',
                'pattern' => $this->s_path,
                'service' => 'Relationship',
                'method' => 'getRelationship',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(
                    'relationshipCode' => array('type' => 'single', 'description' => 'Enter relationship code'),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of relationships',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Relationship.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}