# Ferpa Relationship RESTful Web Service

## Description

restng-ferpa-relationship has been converted into RESTng 2.0 requirements. RESTng 2.0 conversion task focued on fixing syntax or directory structure to meet RESTng 2.0. No functionality or logic change were made.  After conversion was done, PHPUnit has been executed.

## API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/ferpa

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit. 

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatic