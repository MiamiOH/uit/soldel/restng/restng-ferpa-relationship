<?php

namespace MiamiOH\RestngFerpaRelationship\Tests\Unit;
/*
-----------------------------------------------------------
FILE NAME: getRelationshipTest.php

Copyright (c) 2018 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Meenakshi Kandasamy

DESCRIPTION: 


ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit

TABLE USAGE:

Web Service Usage:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

03/12/2018              Kandasm
Description:            Initial Draft
			 
-----------------------------------------------------------
*/

class GetRelationshipTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $api, $request, $dbh, $relationship;

    // set up method automatically called by PHPUnit before every test method:

    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->relationship = new \MiamiOH\RestngFerpaRelationship\Services\Relationship();
        $this->relationship->setDatabase($db);
        $this->relationship->setDatasource($ds);
        $this->relationship->setRequest($this->request);
    }

    /*
      *   Tests Case where no Filter is Given
      *   Actual: No relationship code option specified.
      *	Expected Return: collection of relationship details.
      */
    public function testNoFilter()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllRelationships')));

        $response = $this->relationship->getRelationship();
        $this->assertEquals($this->mockExpectedAllRelationshipResponse(), $response->getPayload());

    }

    public function mockExpectedAllRelationshipResponse()
    {
        $expectedReturn =
            array(
                array(
                    'relationshipCode' => 'A',
                    'relationshipDescription' => 'Sister',
                ),
                array(
                    'relationshipCode' => 'B',
                    'relationshipDescription' => 'Brother',
                ),
                array(
                    'relationshipCode' => 'C',
                    'relationshipDescription' => 'Child',
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryAllRelationships()
    {
        return array(
            array(
                'stvrelt_code' => 'A',
                'stvrelt_desc' => 'Sister',
            ),
            array(
                'stvrelt_code' => 'B',
                'stvrelt_desc' => 'Brother',
            ),
            array(
                'stvrelt_code' => 'C',
                'stvrelt_desc' => 'Child',
            ),
        );
    }

    public function mockNoParameters()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    public function testRelationshipCodeFilter()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsRelationshipFilter')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryRelationshipFilter')));

        $response = $this->relationship->getRelationship();
        $this->assertEquals($this->mockExpectedRelationshipFilterResults(), $response->getPayload());
    }

    public function mockOptionsRelationshipFilter()
    {
        return array(
            'relationshipCode' => 'A'
        );
    }

    public function mockQueryRelationshipFilter()
    {
        return array(
            array(
                'stvrelt_code' => 'A',
                'stvrelt_desc' => 'Sister',
            )
        );
    }

    public function mockExpectedRelationshipFilterResults()
    {
        $expectedReturn =
            array(
                array(
                    'relationshipCode' => 'A',
                    'relationshipDescription' => 'Sister',
                )
            );
        return $expectedReturn;
    }

    public function testRelationshipCodeFilterInvalid()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsRelationshipFilterInvalid')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        try {
            $response = $this->relationship->getRelationship();
            $this->fail();
        } catch (\Exception $e) {
            $this->assertEquals('Error: relationshipCode is invalid', $e->getMessage());
        }
    }

    public function mockOptionsRelationshipFilterInvalid()
    {
        return array(
            'relationshipCode' => 'Invalid'
        );
    }
}